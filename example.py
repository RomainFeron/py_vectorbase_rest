from py_vectorbase_rest import VectorBaseRest
from time import time

species_list = ['aedes_aegypti',
                'aedes_albopictus',
                'anopheles_albimanus',
                'anopheles_arabiensis',
                'anopheles_atroparvus',
                'anopheles_christyi',
                'anopheles_coluzzii',
                'anopheles_culicifacies',
                'anopheles_darlingi',
                'anopheles_dirus',
                'anopheles_epiroticus',
                'anopheles_farauti',
                'anopheles_funestus',
                'anopheles_gambiae',
                'anopheles_maculatus',
                'anopheles_melas',
                'anopheles_merus',
                'anopheles_merus',
                'anopheles_minimus',
                'anopheles_minimus',
                'anopheles_quadriannulatus',
                'anopheles_quadriannulatus',
                'anopheles_sinensis',
                'anopheles_sinensis',
                'anopheles_stephensi',
                'anopheles_stephensi',
                'culex_quinquefasciatus']

requester = VectorBaseRest()

total_start = time()
for species in species_list:
    start = time()
    seq = requester.assembly_info(species=species)
    print(seq['assembly_accession'])
    end = time()
    print('Time : ', end - start)

total_end = time()
print('Total time : ', total_end - total_start)
